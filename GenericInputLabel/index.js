import React from "react";
import { View, TextInput, StyleSheet } from "react-native";
import ResponsiveText from "../ResponsiveText";

export default (props) => {
  return (
    <View style={styles.boxInput}>
      <ResponsiveText title={props.label} h5/>
      <TextInput style={styles.input} {...props} />
    </View>
  );
};

const styles = StyleSheet.create({
  boxInput: {
    marginVertical: 2.5,
  },

  input: {
    borderWidth: 1,
    borderColor: "#c1c1c1",
    borderRadius: 5,
    paddingHorizontal: 5,
    paddingVertical: 2.5,
  },
});
