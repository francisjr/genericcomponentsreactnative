import React from "react";
import { Text } from "react-native";

import adjust from "./adjust";

export default ({
  h1,
  h2,
  h3,
  h4,
  h5,
  p,
  bold,
  italic,
  title,
  style,
  ...rest
}) => {
  return (
    <Text
      style={[
        h1 && { fontSize: adjust(30) },
        h2 && { fontSize: adjust(24) },
        h3 && { fontSize: adjust(22) },
        h4 && { fontSize: adjust(20) },
        h5 && { fontSize: adjust(18) },
        p && { fontSize: adjust(16) },
        bold && { fontWeight: "bold" },
        italic && { fontStyle: "italic" },
        style,
      ]}
      {...rest}
    >
      {title}
    </Text>
  );
};
