import React, { useState } from "react";
import { View } from "react-native";
import GenericInputLabel from "../GenericInputLabel";

export default (props) => {
  return (
    <View>
      <GenericInputLabel
        {...props}
        label={defineLanguage(props.language)}
        secureTextEntry={true}
      />
    </View>
  );
};

const defineLanguage = (language) => {
  return { "pt-BR": "Senha", "en-US": "Password" }[language] ?? "Password";
};
